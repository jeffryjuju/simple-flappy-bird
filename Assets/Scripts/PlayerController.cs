﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the player's control system.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))] // This is used so that whenever the script is implemeneted to a game object, the game object will instantly add Rigidbody2D component to the game object

public class PlayerController : MonoBehaviour
{
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnPlayerDied;
    public static event PlayerDelegate OnPlayerScored;

    public float BirdForce = 10;
    public float TiltSmooth = 5;
    public Vector3 StartPosition;

    Rigidbody2D rigidbody;
    Quaternion downRotation; // Quaternion is an angular data type
    Quaternion forwardRotation;
    
    GameManager gameManager;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        downRotation = Quaternion.Euler(0, 0, -90);
        forwardRotation = Quaternion.Euler(0, 0, 35);
        gameManager = GameManager.Instance;
    }

    void OnEnable()
    {
        GameManager.OnGameStarted += OnGameStarted;
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    void OnDisable()
    {
        GameManager.OnGameStarted -= OnGameStarted;
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
    }
    
    /// <summary>
    /// This method is used to reset the bird's state and velocity.
    /// </summary>
    private void OnGameStarted()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.simulated = true;
    }

    /// <summary>
    /// This method is used to reset the bird's position and rotation.
    /// </summary>
    private void OnGameOverConfirmed()
    {
        transform.localPosition = StartPosition;
        transform.rotation = Quaternion.identity;
    }

    /// <summary>
    /// This method will control the bird's angle.
    /// The bird will tilt upwards whenever the player press LMB.
    /// The bird will always tilt downwards if no action is performed.
    /// </summary>
    void Update()
    {
        // If nothing happens, the game stop updating (used for the Countdown phase)
        if (gameManager.GameOver) return; 

        if (Input.GetMouseButtonDown(0))
        {
            transform.rotation = forwardRotation;
            rigidbody.velocity = Vector3.zero;
            rigidbody.AddForce(Vector2.up * BirdForce, ForceMode2D.Force);
        }

        // Lerp will rotate the bird downwards slowly.
        // Quaternion.Lerp(source value, target value, angular speed)
        transform.rotation = Quaternion.Lerp(transform.rotation, downRotation, TiltSmooth * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "ScoreZone")
        {
            // This will trigger a score event
            OnPlayerScored(); // This event will be sent to the GameManager
        }

        if(collision.gameObject.tag == "DeadZone")
        {
            // This will trigger a dead event
            rigidbody.simulated = false; // This will freeze the game whenever the player dies
            OnPlayerDied(); // This event will be sent to the GameManager
        }
    }
}
