﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class will control various screen changes and responses on the game, 
/// such as going to StartPage, GameOverPage, CountdownPage.
/// </summary>
public class GameManager : MonoBehaviour
{
    public delegate void GameDelegate();
    public static event GameDelegate OnGameStarted;
    public static event GameDelegate OnGameOverConfirmed;

    public static GameManager Instance; // This is used so that other classes can access the GameManager class
    
    public GameObject StartPage;
    public GameObject GameOverPage;
    public GameObject CountdownPage;
    public Text ScoreText;
    
    enum PageState
    {
        None,
        Start,
        GameOver,
        Countdown
    }

    int score = 0;
    bool gameOver = false;

    public bool GameOver
    {
        get 
        {
            return gameOver;
        }
    }

    public int Score
    {
        get
        {
            return score;
        }
    }

    void Awake()
    {
        Instance = this;
    }

    void OnEnable()
    {
        CountdownText.OnCountdownFinished += OnCountdownFinished;
        PlayerController.OnPlayerScored += OnPlayerScored;
        PlayerController.OnPlayerDied += OnPlayerDied;
    }

    void OnDisable()
    {
        CountdownText.OnCountdownFinished -= OnCountdownFinished;
        PlayerController.OnPlayerScored -= OnPlayerScored;
        PlayerController.OnPlayerDied -= OnPlayerDied;
    }

    /// <summary>
    /// Whenever the timer is done, the game will start.
    /// </summary>
    private void OnCountdownFinished()
    {
        SetPageState(PageState.None);
        OnGameStarted();
        score = 0;
        gameOver = false;
    }

    /// <summary>
    /// This method is used to add the score.
    /// </summary>
    private void OnPlayerScored()
    {
        score++;
        ScoreText.text = score.ToString();
    }

    /// <summary>
    /// This method is used to finalize the score and compare it with the HighScore.
    /// This method will also change the PageState to GameOverPage.
    /// </summary>
    private void OnPlayerDied()
    {
        gameOver = true;
        int savedScore = PlayerPrefs.GetInt("HighScore");
        if (score > savedScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        SetPageState(PageState.GameOver);
    }

    /// <summary>
    /// This method is used to change the UI.
    /// </summary>
    /// <param name="state"></param>
    void SetPageState (PageState state)
    {
        switch (state)
        {
            case PageState.None:
                StartPage.SetActive(false);
                GameOverPage.SetActive(false);
                CountdownPage.SetActive(false);
                break;
            case PageState.Start:
                StartPage.SetActive(true);
                GameOverPage.SetActive(false);
                CountdownPage.SetActive(false);
                break;
            case PageState.GameOver:
                StartPage.SetActive(false);
                GameOverPage.SetActive(true);
                CountdownPage.SetActive(false);
                break;
            case PageState.Countdown:
                StartPage.SetActive(false);
                GameOverPage.SetActive(false);
                CountdownPage.SetActive(true);
                break;
        }
    }
    
    /// <summary>
    /// This method is activated when the play button is hit.
    /// </summary>
    public void StartGame()
    {
        SetPageState(PageState.Countdown); // The game will start the countdown when the play button is hit
    }

    /// <summary>
    /// This method is activated when the replay button is hit.
    /// </summary>
    public void ConfirmGameOver()
    {
        OnGameOverConfirmed(); // This is an event
        ScoreText.text = "0";
        SetPageState(PageState.Start); // The game will change to StartPage when the replay button is hit
    }
}
