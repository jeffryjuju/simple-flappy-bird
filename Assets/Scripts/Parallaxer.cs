﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Parallaxer : MonoBehaviour
{
    /// <summary>
    /// Instead of instatiating and destroying GameObject(s), creating PoolObject is better for game performance.
    /// Reference: https://docs.unity3d.com/Manual/MobileOptimizationPracticalScriptingOptimizations.html
    /// Reference: https://www.raywenderlich.com/847-object-pooling-in-unity#toc-anchor-009
    /// </summary>
    class PoolObject
    {
        public Transform transform;
        public bool inUse; // The state of the PoolObject
        public PoolObject (Transform t)
        {
            transform = t;
        }
        public void Use() 
        {
            inUse = true;
        }
        public void Dispose()
        {
            inUse = false;
        }
    }

    /// <summary>
    /// This struct controls the minimum and maximum y position when spawning obstacles.
    /// </summary>
    [System.Serializable] 
    public struct YSpawnRange 
    {
        public float minimum; // Minimum obstacle height
        public float maximum; // Maximum obstacle height
    }

    public GameObject prefab; // This property determines which prefabs that is going to be parallaxed
    public int poolSize; // The size of your pool (quantity of objects that the pool contains).
    public float shiftSpeed; // The shift speed of the obstacle
    public float spawnRate; // Spawn rate of the obstacle

    public YSpawnRange ySpawnRange;
    public Vector3 defaultSpawnPosition;
    public bool spawnImmediate;
    public Vector3 immediateSpawnPosition;
    public Vector2 targetAspectRatio;

    float spawnTimer;
    float targetAspect;
    PoolObject[] poolObjects;
    GameManager gameManager;

    void Awake()
    {
        Configure();
    }

    void Start()
    {
        gameManager = GameManager.Instance;
    }

    void OnEnable()
    {
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    void OnDisable()
    {
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
    }

    /// <summary>
    /// This method will be initiated when game is over.
    /// This method will dispose all poolObjects[] from the game.
    /// </summary>
    void OnGameOverConfirmed()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].Dispose();
            poolObjects[i].transform.position = Vector3.one * 1000;
        }
    }

    void Update()
    {
        if (gameManager.GameOver) return;

        Shift();
        spawnTimer += Time.deltaTime; // This controls the spawnTimer
        if (spawnTimer > spawnRate) // Whenever the spawnTimer reach spawnRate value, it will spawn an obstacle
        {
            Spawn();
            spawnTimer = 0; // Resets the spawnTimer to spawn the next obtacle
        }
    }

    /// <summary>
    /// This method configures all necessary properties when Awake() is called (at the start of the game).
    /// </summary>
    void Configure()
    {
        targetAspect = targetAspectRatio.x / targetAspectRatio.y;
        poolObjects = new PoolObject[poolSize];
        for (int i = 0; i < poolObjects.Length; i++) // This creates the gameObject into the poolObjects[]
        {
            GameObject gameObject = Instantiate(prefab) as GameObject;
            Transform t = gameObject.transform;
            t.SetParent(transform); // We do this because we're implementing the Parallaxer script on an empty GameObject (as parent)
            t.position = Vector3.one * 1000;
            poolObjects[i] = new PoolObject(t); // inserts the PoolObject(t) to poolObjects[]
        }

        if (spawnImmediate)
        {
            SpawnImmediate();
        }
    }

    /// <summary>
    /// This method controls the spawning mechanism.
    /// </summary>
    void Spawn()
    {
        Transform t = GetPoolObject();
        if (t == null) return; // if this happen, the poolSize is too small. Consider adding poolSize
        Vector3 position = Vector3.zero;
        position.x = (defaultSpawnPosition.x * Camera.main.aspect) / targetAspect;
        position.y = Random.Range(ySpawnRange.minimum, ySpawnRange.maximum);
        t.position = position;
    }

    /// <summary>
    /// This method is used for pre-warming the object particle.
    /// </summary>
    void SpawnImmediate()
    {
        Transform t = GetPoolObject();
        if (t == null) return; // if this happen, the poolSize is too small. Consider adding poolSize
        Vector3 position = Vector3.zero;
        position.x = (defaultSpawnPosition.x * Camera.main.aspect) / targetAspect;
        position.y = Random.Range(ySpawnRange.minimum, ySpawnRange.maximum);
        t.position = position;
        Spawn();
    }

    /// <summary>
    /// This is the shifting method for the obstacles.
    /// </summary>
    void Shift()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].transform.localPosition += -Vector3.right * shiftSpeed * Time.deltaTime;
            CheckDisposeObject(poolObjects[i]);
        }
    }


    /// <summary>
    /// This method is used to check whether the PoolObject is in used or need to be disposed.
    /// </summary>
    /// <param name="poolObject"></param>
    void CheckDisposeObject(PoolObject poolObject)
    {
        if (poolObject.transform.position.x < (-defaultSpawnPosition.x * Camera.main.aspect) / targetAspect)
        {
            poolObject.Dispose();
            poolObject.transform.position = Vector3.one * 1000;
        }
    }

    /// <summary>
    /// This is used to use an idle poolObjects[] and put it into the game world.
    /// </summary>
    /// <returns></returns>
    Transform GetPoolObject()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            if (!poolObjects[i].inUse)
            {
                poolObjects[i].Use(); // Change the state inUse to true
                return poolObjects[i].transform;
            }
        }
        return null;
    }
}
