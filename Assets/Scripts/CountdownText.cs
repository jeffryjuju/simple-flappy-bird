﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used for the countdown phase.
/// </summary>
[RequireComponent(typeof(Text))] // Everytime this class is used, it will need add a Text component to the GameObject
public class CountdownText : MonoBehaviour
{
    public delegate void CountdownDelegate();
    public static event CountdownDelegate OnCountdownFinished;

    Text countdown;

    void OnEnable()
    {
        countdown = GetComponent<Text>();
        countdown.text = "3";
        StartCoroutine("Countdown");
    }

    /// <summary>
    /// This method is used for the countdown decrement process.
    /// </summary>
    /// <returns></returns>
    IEnumerator Countdown()
    {
        int count = 3;
        for (int i = 0; i < count; i++)
        {
            countdown.text = (count - i).ToString();
            yield return new WaitForSeconds(1);
        }

        OnCountdownFinished();
    }
}
