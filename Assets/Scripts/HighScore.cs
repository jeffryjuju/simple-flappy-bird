﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used to get the HighScore value from PlayerPrefs
/// </summary>
[RequireComponent(typeof(Text))]
public class HighScore : MonoBehaviour
{
    Text highScore;

    void OnEnable()
    {
        highScore = GetComponent<Text>();
        highScore.text = $"Highscore: {PlayerPrefs.GetInt("HighScore").ToString()}";
    }
}
